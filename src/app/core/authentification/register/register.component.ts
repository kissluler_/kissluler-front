import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { TokenStorageService } from '../../services/token-storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  username = '';
  email = '';
  password = '';
  passwordRepeat = '';
  hasError = false;
  hidePassword = true;
  hideRepeatPassword = true;
  loading = false;

  constructor(
    private auth: AuthService,
    private router: Router,
    private tokenStorage: TokenStorageService,
  ) { }

  register() {
    this.hasError = false;
    this.auth.register({ username: this.email, email: this.email, password: this.password }).subscribe({
      next: () => {
        this.router.navigate(['/'])
      },
      error: () => this.hasError = true
    });

  }
}

