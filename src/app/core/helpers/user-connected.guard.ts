import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { AuthService } from "../services/auth.service";

@Injectable({
    providedIn: 'root',
})
export class UserConnectedGuard implements CanActivate {

    constructor(
        private authService: AuthService,
        private router: Router) { }

    canActivate() {
        const authUser = this.authService.state.user;
        const authUserAuthorities = this.authService.state.authorities;
        if (authUser && authUserAuthorities && authUserAuthorities == "ROLE_USER") {
            return true;
        }
        this.router.navigate(["/"]);
        return false;
    }

}