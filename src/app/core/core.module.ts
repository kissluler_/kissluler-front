import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './authentification/login/login.component';
import { RegisterComponent } from './authentification/register/register.component';
import { LegalComponent } from './legal/legal.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { CkiesParamsComponent } from './ckies-params/ckies-params.component';
import { CkiesDialogComponent } from './ckies-params/ckies-dialog/ckies-dialog.component';

@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        LegalComponent,
        TermsComponent,
        PrivacyComponent,
        CkiesParamsComponent,
        CkiesDialogComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
    ],
    providers: []
})
export class CoreModule { }
