import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CkiesParamsComponent } from './ckies-params.component';

describe('CkiesParamsComponent', () => {
  let component: CkiesParamsComponent;
  let fixture: ComponentFixture<CkiesParamsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CkiesParamsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CkiesParamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
