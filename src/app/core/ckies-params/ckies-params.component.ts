import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CkiesDialogComponent } from './ckies-dialog/ckies-dialog.component';

@Component({
  selector: 'app-ckies-params',
  templateUrl: './ckies-params.component.html',
  styleUrls: ['./ckies-params.component.scss']
})
export class CkiesParamsComponent {
  
  constructor(
    public dialog: MatDialog
  ) { }

  openDialog() {
    const dialogRef = this.dialog.open(CkiesDialogComponent, {
      width: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}

