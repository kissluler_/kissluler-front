import { ICategory } from './category.model';
export interface IProject {
    id?: number;
    name: string;
    catchPhrase: string;
    description?: string;
    media: string;
    backers: number;
    fundRaised: number;
    fundingGoal: number;
    creationDate?: any;
    endDate?: any;
    daysToGo: number;
    levels?: string;
    categoryList: ICategory[];
}


export interface Projects {
    projects: IProject[];
}