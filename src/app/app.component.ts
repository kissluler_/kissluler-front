import { Component } from '@angular/core';
import { AuthService } from './core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isLogged: boolean = false;
  title = 'kissluler-front';
  username? = "";

  constructor(
    private router: Router, private auth: AuthService) { }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event.constructor.name === "NavigationEnd") {
        this.auth.state.user ? this.isLogged = true : this.isLogged = false;
        this.username = this.auth.state.user?.username;
      }
    })
  }

  logout() {
    this.auth.logout().pipe().subscribe({
        complete: () => this.router.navigate(["/login"])
      });
  }
}
