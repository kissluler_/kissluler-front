import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { ProjectsModule } from '../projects/projects.module';

@NgModule({
    declarations: [
        HomeComponent,
    ],
    imports: [
        HomeRoutingModule,
        CommonModule,
        SharedModule,
        ProjectsModule,
    ],
    providers: []
})
export class HomeModule { }
