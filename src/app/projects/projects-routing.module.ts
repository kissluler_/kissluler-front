import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProjectsComponent } from "./projects.component";
import { ProjectDetailComponent } from './project-detail/project-detail.component';

const projectsRoutes: Routes = [
    { path: 'projects', component: ProjectsComponent },
    { path: 'project/:id', component: ProjectDetailComponent},
];

@NgModule({
    imports: [RouterModule.forChild(projectsRoutes)],
    exports: [RouterModule]
})
export class ProjectsRoutingModule { }