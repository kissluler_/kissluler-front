import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ProjectService } from '../project.service';
import { ICategory } from '../../interfaces/category.model';
import { tap, take, reduce, EMPTY } from 'rxjs';

@Component({
  selector: 'app-project-categories',
  templateUrl: './project-categories.component.html',
  styleUrls: ['./project-categories.component.scss']
})
export class ProjectCategoriesComponent implements OnInit {
  public categoryList!: ICategory[];
  public selectedCategory: ICategory | undefined;
  @Output()
  category: EventEmitter<any> = new EventEmitter();

  constructor(public projectService: ProjectService
  ) { }

  getCategoryList() {
    this.projectService.getCategoryList()
      .pipe(tap((data: any) => {
        this.categoryList = data;
      }), take(1))
      .subscribe();
  }

  public selectCategory(category: ICategory) {
    if (this.selectedCategory == category) {
      this.selectedCategory = undefined;
      this.category.emit(this.selectedCategory);
    } else {
      this.selectedCategory = category;
      this.category.emit(category);
    }
  }

  ngOnInit() {
    this.getCategoryList();
  }

  public getColor(category: ICategory) {
    if (this.selectedCategory == category) {
      return 'white';
    } else { return 'black'; }
  }

  public getBackgroundColor(category: ICategory) {
    if (this.selectedCategory == category) {
      return 'var(--color-secondary-light) !important';
    } else { return '#fff !important'; }
  }

  public getBorderColor(category: ICategory) {
    if (this.selectedCategory == category) {
    return '1px solid var(--color-secondary)';
  } else { return '1px solid var(--color-primary)'; }

  }

}
