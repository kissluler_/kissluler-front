import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectsComponent } from './projects.component';
import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectCategoriesComponent } from './project-categories/project-categories.component';

@NgModule({
    declarations: [
        ProjectsComponent,
        ProjectDetailComponent,
        ProjectCategoriesComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        ProjectsRoutingModule
    ],
    exports: [
        ProjectsComponent,
    ],
    providers: []
})
export class ProjectsModule { }
