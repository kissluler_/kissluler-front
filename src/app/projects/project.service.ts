import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { IProject } from 'src/app/interfaces/project.model';
import { ICategory } from '../interfaces/category.model';

@Injectable({
    providedIn: 'root'
  })
  export class ProjectService {
  
    private projectUrl = '/api/projects';
    private categoryUrl = '/api/category';
  
    constructor(private http: HttpClient) { }
  
    getProjectById(id: number): Observable<IProject> {
      return this.http.get<IProject>(`${this.projectUrl}/${id}`);
    }
  
    getProjectsList(): Observable<IProject[]> {
      return this.http.get<IProject[]>(`${this.projectUrl}`);
    }
  
    postProject(username: string, Project: IProject): Observable<IProject> {
      return this.http.post<IProject>(`${this.projectUrl}/create-project/${username}`, Project);
    }
  
    updateProject(id: number, Project: IProject): Observable<IProject> {
      return this.http.put<IProject>(`${this.projectUrl}/${id}`, Project);
    }
  
    updateProjectActive(id: number, active: boolean): Observable<IProject> {
      return this.http.put<IProject>(`${this.projectUrl}/update-active/${id}`, active);
    }
  
    deleteProject(id: number): Observable<any> {
      return this.http.delete<IProject>(`${this.projectUrl}/${id}`);
    }
  
    applyToProject(id_offer: number, username: string, value: any): Observable<any> {
      return this.http.post(`${this.projectUrl}/apply/${id_offer}/${username}`, value);
    }

    getCategoryList(): Observable<ICategory[]> {
      return this.http.get<ICategory[]>(`${this.categoryUrl}`);
    }
  }
  