import {
  Component,
  Input, OnInit,
  ViewChild
} from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { take, tap } from 'rxjs';
import { ICategory } from '../interfaces/category.model';
import { IProject } from '../interfaces/project.model';
import { ProjectService } from './project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
})
export class ProjectsComponent implements OnInit {
  @Input()
  sliceEnd: number = 0;

  isOnProjectsPage: boolean = false;

  public projects!: IProject[];

  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;
  pageSizeOptions: number[] = [5, 10, 25, 50];
  pageEvent!: PageEvent;
  public activePageDataChunk: IProject[] = [];
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private projectService: ProjectService,
    private router: Router,
  ) { }

  public handlePage(e: any): void {
    const firstCut = e.pageIndex * e.pageSize;
    const secondCut = firstCut + e.pageSize;
    this.activePageDataChunk = this.projects.slice(firstCut, secondCut);
    window.scroll(0, 300);
  }

  ngOnInit(): void {
    if (this.router.url.indexOf('/projects') > -1) {
      this.isOnProjectsPage = true;
    }
    this.getProjectsList();
  }

  private getProjectsList(): void {
    this.projectService
      .getProjectsList()
      .pipe(
        tap((data) => {
          this.projects = data.filter(d => d.daysToGo >= 0);
          this.totalSize = this.projects.length;
          this.activePageDataChunk = !this.isOnProjectsPage ? this.projects.slice(0, 8) : this.projects.slice(0, this.pageSize);
          if (this.sliceEnd == 0) {
            this.sliceEnd = data.length;
          }
        }),
        take(1)
      )
      .subscribe();
  }

  public filterProjectsList(event: any): void {
    let selectedCategory: ICategory = event;
    let filteredProjects
    if (this.projects !== undefined) {
      filteredProjects = this.projects.filter((project: IProject) => {
        const categoryIds = project.categoryList.map(category => category.id);
        if (selectedCategory !== undefined) {
          if (!categoryIds.includes(selectedCategory.id)) {
            return false;
          }
        } else {
          filteredProjects = this.projects;
        }
        return true;
      });

      this.activePageDataChunk = filteredProjects.slice(0, this.pageSize);
      this.currentPage = 0;
      this.paginator.pageIndex = 0;
      this.totalSize = filteredProjects.length;
    }
  }

  public setPageSizeOptions(setPageSizeOptionsInput: string): void {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  public checkFundingGoal(goal: number, raised: number) {
    const result = (raised * 100) / goal;
    return result.toFixed(2);
  }
}
