# Kissluler Front Grand Public

Nouvelle génération de site de financement participatif en ligne.

# Liens de conception
- [LucidChart - Diagramme de cas d'utilisations](https://lucid.app/lucidchart/8c4d5de4-8ce6-4e0b-826c-37dde6668ae4/edit?existing=1&token=e6f73d0ff0709e76275ca06a5da32052a905426a-eml%3Demerit.florian%2540gmail.com%26ts%3D1655124258%26uid%3D167266115&docId=8c4d5de4-8ce6-4e0b-826c-37dde6668ae4&shared=true&invitationId=inv_004df356-a1a3-4838-a419-80066772e6b6&page=0_0#)
- [LucidChart - Diagramme d'entité](https://lucid.app/lucidchart/ac17e23d-aa10-40bb-a1d3-b21aa4decdf6/edit?existing=1&token=882bb7e56360ea437cf97a2b11cc3a0cc59dbd93-eml%3Dkevin.radlowski%2540gmail.com%26ts%3D1655131013%26uid%3D167271776&docId=ac17e23d-aa10-40bb-a1d3-b21aa4decdf6&shared=true&page=0_0#)
- [LucidChart - Diagramme de classe](https://lucid.app/lucidchart/d20565b6-0eb7-456e-be66-5cfa373e1645/edit?existing=1&token=54405ebfb24e2458edb71977c346aa46cd680246-eml%3Dkevin.radlowski%2540gmail.com%26ts%3D1655216406%26uid%3D167271776&docId=d20565b6-0eb7-456e-be66-5cfa373e1645&shared=true&page=0_0#)


### Fonctionnalités prévues

- Créer un compte
- Créer un projet (photo, description, montant minimal demandé, contreparties, date de fin)
- Financer un projet et recevoir ses contreparties
- Panneau d'administration permettant de modérer les projets

### Fonctionnalités optionnelles

-Pouvoir trier les projets par catégories
- Pouvoir Signaler un projet à l'équipe de modération, avec un message
- Pouvoir suspendre/relancer/prolonger/annuler (avec remboursement) un projet
- Gérer les paliers supplémentaires de financement
...
​
# Stack Technique

Le site est fait en techno web, avec un back en mode API, un front "grand public", et un front "admin".
Accès aux différents codes sources :
- [Front grand public](https://gitlab.com/kissluler_/kissluler-front)
- [Front admin](https://gitlab.com/kissluler_/kissluler-front-admin)
- [Back API](https://gitlab.com/kissluler_/kissluler-api)

Nous utilisons Java 17 et Spring 2.4 pour la partie API, ainsi que Angular 13 pour les parties front.
Les librairies Material Angular et Flex Layout seront aussi utilisées.
​

Fonctionnalités supplémentaires possibles (laissez libre court à votre imagination !) :

